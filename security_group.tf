resource "aws_security_group" "alb" {
  name        = "scg-${var.name}"
  description = "${var.name} HTTPS/HTTP access"
  vpc_id      = "${data.aws_subnet.subnet.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "security_group" {
  value = {
    id          = "${aws_security_group.alb.id}",
    vpc_id      = "${aws_security_group.alb.vpc_id}"
    owner_id    = "${aws_security_group.alb.owner_id}"
    name        = "${aws_security_group.alb.name}"
    description = "${aws_security_group.alb.description}"
    ingress     = "${aws_security_group.alb.ingress}"
    egress      = "${aws_security_group.alb.egress}"
  }
}
