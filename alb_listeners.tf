resource "aws_alb_listener" "listener" {
  depends_on        = [ "aws_alb_target_group.target_group" ]
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "${var.listener_port}"
  protocol          = "${var.listener_protocol}"
  default_action {
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
    type             = "forward"
  }
  ssl_policy      = "ELBSecurityPolicy-2015-05"
  certificate_arn = "${var.certificate_arn}"
}

output "listener" {
  value = {
    id  = "${aws_alb_listener.listener.id}"
    arn = "${aws_alb_listener.listener.arn}"
  }
}

resource "aws_alb_listener_rule" "rule" {
  listener_arn = "${aws_alb_listener.listener.arn}"
  priority     = "${var.listener_rule_priority}"
  action {
    type = "forward"
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
  }
  condition {
    field = "path-pattern"
    values = [ "${var.listener_rule_condition_values}" ]
  }
}
